// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('extension "materials" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('materials.materialEditor', () => {
		// The code you place here will be executed every time your command is executed

		const panel = vscode.window.createWebviewPanel(
			'materialEditor', // Identifies the type of the webview. Used internally
			'Material Editor', // Title of the panel displayed to the user
			vscode.ViewColumn.One, // Editor column to show the new webview panel in.
			{
				// Only allow the webview to access resources in our extension's media directory
				localResourceRoots: [vscode.Uri.file(path.join(context.extensionPath, 'media'))],
				enableScripts: true,
			}
		);
		
		let onDiskPath = vscode.Uri.file(path.join(context.extensionPath, 'media')).with({
			scheme: "vscode-resource"
		}).toString();

		const styleSrc = vscode.Uri.file(path.join(context.extensionPath, 'media')).with({ scheme: 'vscode-resource' });

		panel.webview.html = generateWebviewContent(onDiskPath);
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}

function generateWebviewContent(onDiskPath: string) 
{
	// Yey, back to the 2000!
	// Server side scripting of html!
	return `
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="${onDiskPath}/litegraph.css">
	<script type="text/javascript" src="${onDiskPath}/litegraph.js"></script>
	<title>Material Editor</title>
	<style>
	.body {
		width:100%; 
		height:100%;
	}
	#graphCanvas {
	}
	</style>
</head>
<body>
	<canvas id='graphCanvas'  width='1024' height='720' ></canvas>

	<script>
		var data = ${generateJsData()};

		var graph = new LGraph();

		var canvas = new LGraphCanvas("#graphCanvas", graph);

		var node_const = LiteGraph.createNode("basic/const");
		node_const.pos = [200,200];
		graph.add(node_const);
		node_const.setValue(4.5);

		var node_watch = LiteGraph.createNode("basic/watch");
		node_watch.pos = [700,200];
		graph.add(node_watch);

		node_const.connect(0, node_watch, 0 );

		graph.start()
	</script>
</body>
</html>`;
}

function generateJsData()
{
	return `
	{
		"nodes": [
			{
				"id": 0,
				"type": "texture",
				"value": "hello.png",
				"color": "srgba8",
			},
			{
				"id": 1,
				"type": "texture",
				"value": "hello2.png",
				"color": "srgba8",
			},
			{
				"id": 3,
				"type": "material",
				"input": {
					"albedo": 0,
					"alpha": 1
				}
			}
		]
	}
	`;
}
